# List of projects

Projects for the final examination are related to the following topics: COVID 19, Software Metrics and Log Analysis.

**Note**: Each project can be developed by one student or by a small group of 2/3 students.  

Identifier|Title|Year|URL|Data|State|Badge Number(BN)[1,3]
-|-|-|-|-|-|-
Project (PR) Identifier| |Year of the publication specified in the URL|Link to the project|Available|Available|University Badge Numbers of students involved in the project
|||||Contact Professor|Not Available|
||||||Done|


COVID 19

Identifier|Title|Year|URL|Data|State|BN1|BN2|BN3 
-|-|-|-|-|-|-|-|-
PR1|Association of NO2 levels with SARS-COV-2|2020|[SAtellite-detected tropospheric nitrogen dioxide and spread of SARS-CoV-2 infection in Northern Italy](https://www.sciencedirect.com/science/article/pii/S0048969720337992)|Available|Not Available|1900095930||
PR2|Association of NO2 levels with SARS-COV-2|2021|[Associations between mortality from COVID-19 in two Italian regions and outdoor air pollution as assessed through tropospheric nitrogen dioxide](https://www.sciencedirect.com/science/article/pii/S0048969720368868)|Available|Not Available|1057715|1032944|1058296
PR3|Effect of the COVID-19 lockdown on pollution in CZ|2021|[Effect of the COVID-19 Lockdown on Air Pollution in the Ostrava Region](https://www.mdpi.com/1660-4601/18/16/8265)|Contact Professor|Not Avialbale|0001057217|0001034982|0001057681
PR4|Sentiment Analysis for COVID-19 Twitter Data|2020|[Sentiment Analysis in Indian Sub-continent During COVID-19 Second Wave using Twitter Data](https://ieee-dataport.org/documents/covid-19-second-wave-tweets-annotations)|Contact Professor|Not Available|1036045|NA|NA
PR5|Tweets about coronavirus pandemic events|2020|[Predicting Coronavirus Pandemic in Real-Time Using Machine Learning and Big Data Streaming System](https://www.hindawi.com/journals/complexity/2020/6688912/)|Contact Professor|Not Available|1024924|1025999|NA
PR6|Seasonability of coronavirus disease|2020|[Temperature, Humidity, and Latitude Analysis to Estimate Potential Spread and Seasonability of Coronavirus Disease 2019 (COVID-19)](https://jamanetwork.com/journals/jamanetworkopen/fullarticle/2767010)|Contact Professor|Not Available|001035754|0001026530|0001028507
PR7|Relationship between PM2.5 and PM10 impact on COVID-19|2020|[Assessing the relationship between surface levels of PM2.5 and PM10 particulate matter impact on COVID-19 in Milan, Italy](https://www.sciencedirect.com/science/article/pii/S0048969720333453)|Contact Professor|Not Available|1025608|1028021|1026036 
PR8|Role of chronic air pollution levels in COVID-19|2020|[Role of the chronic air pollution levels in the COVID-19 outbreak risk in Italy](https://www.sciencedirect.com/science/article/pii/S0269749120332115)|Contact Professor|Not Available|0001036146|0001034281|0001046500
PR9|Pollution and COVID-19 pandemic in California|2020|[Correlation between environmental pollution indicators and COVID-19 pandemic: A brief study in Californian context](https://www.sciencedirect.com/science/article/pii/S0013935120305454?via%3Dihub)|Contact Professor|Not Available|1036547|1034898|1049441
PR10|Clustering techniques|2021|[A comparative study of clustering techniques applied on COVID-19 scientific literature](https://ieeexplore.ieee.org/document/9340213)|Contact Professor|Not Available|1034853|1027021|NA
PR11|Climate indicators and COVID-19 pandemic in New York|2020|[Correlation between climate indicators and COVID-19 pandemic in New York, USA](https://www.sciencedirect.com/science/article/pii/S0048969720323524#:~:text=Climate%20indicators%20are%20integral%20in,useful%20in%20suppressing%20COVID%2D19.)|Contact Professor|Not Available|1025474|NA|NA
PR12|Particular Matter and COVID-19|2020|[Particulate matter pollution and the COVID-19 outbreak: results from Italian regions and provinces](https://www.archivesofmedicalscience.com/Particulate-matter-pollution-and-the-COVID-19-outbreak-results-from-Italian-regions,122328,0,2.html)|Contact Professor|Not Available|0001042345|0000973616|NA
PR13|Pollutants and COVID-19|2020|[Air pollutants and risks of death due to COVID-19 in Italy](https://www.sciencedirect.com/science/article/pii/S0013935120313566)|Contact Professor|Not Available|0001043421|0001043418|NA
PR27|ML and COVID 19|2020|[Applications of machine learning and artificial intelligence for Covid-19 (SARS-CoV-2) pandemic: A review](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7315944/)|Contact Professor|Available|||
PR28|Cluster analysis|2021|[The Use of Cluster Analysis to Evaluate the Impact of COVID-19 Pandemic on Daily Water Demand Patterns](https://www.mdpi.com/2071-1050/13/11/5772/pdf)|Contact Professor|Available|||

SOFTWARE METRICS

Identifier|Title|Year|URL|Data|State|BN1|BN2|BN3 
-|-|-|-|-|-|-|-|-
PR14|Software Defect Prediction|2021|[Software Defect Prediction for Healthcare Big Data: An Empirical Evaluation of Machine Learning Techniques](https://www.hindawi.com/journals/jhe/2021/8899263/)|Available|Not Available|0001025642|NA|NA
PR15|Software Complexity Prediction|2016|[Software Complexity Prediction by Using Basic Attributes](https://ijaers.com/detail/software-complexity-prediction-by-using-basic-attributes-2/)|Available|Not Available|0001026186|NA|NA
PR16|Developer Modelling|2017|[Developer Modelling using Software Quality Metrics and Machine Learning](https://www.scitepress.org/Papers/2017/63271/63271.pdf)|Available|Available|||
PR17|Defect Prediction|2020|[Software Defect Prediction Using Machine Learning](https://ieeexplore.ieee.org/document/9142909)|Available|Not Available|1030584||
PR18|Cross-project defect prediction|2016|[Cross-Project Defect Prediction Using a Connectivity-Based Unsupervised Classifier](https://seal-queensu.github.io/publications/pdf/ICSE-Feng-2016.pdf)|Available|Available|||
PR19|Predicting Software Defects|2021|[A Survey on Data Science Techniques for Predicting Software Defects](https://link.springer.com/chapter/10.1007/978-3-030-75078-7_31)|Available|Available|||
PR20|Defect Predictions|2020|[Understanding Machine Learning Software Defect Predictions](https://link.springer.com/article/10.1007/s10515-020-00277-4)|Available|Available|||
PR21|Unlabelled datasets|2015|[CLAMI: Defect Prediction on Unlabeled Datasets T](https://ieeexplore.ieee.org/document/7372033)|Available|Available|||
PR22|Complexity Prediction|2020|[Performance Analysis of Machine Learning Approaches in Software Complexity Prediction](https://link.springer.com/chapter/10.1007%2F978-981-33-4673-4_3)|Available|Available|||
PR23|Software Maintainability Predictions|2019|[Deep Learning Approach for Software Maintainability Metrics Prediction](https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=8698760)|Available|Available|||
PR24|Complex Networks|2020|[Structure, Dynamics, and Applications of Complex Networks in Software Engineering](https://www.hindawi.com/journals/mpe/2020/6038619/)|Contact Professor|Available|||

LOG ANALYSIS

Identifier|Title|Year|URL|Data|State|BN1|BN2|BN3 
-|-|-|-|-|-|-|-|-
PR25|Automatic log analysis|2019|[Automatic log analysis with NLP for the CMS workflow handling](https://www.epj-conferences.org/articles/epjconf/abs/2020/21/epjconf_chep2020_03006/epjconf_chep2020_03006.html)|Contact Professor|Not Avialbale|0001034719|NA|NA
PR26|Log Analysis with NLP|2017|[Experience Report: Log Mining using Natural Language processing and Application to anomaly detection](https://hal.laas.fr/hal-01576291/file/PID4955309.pdf)|Contact Professor|Avialbale|||
